import java.util.*;

public class XO {
	public static Scanner kb = new Scanner(System.in);
	public static char[][] XOgame = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	public static char turn = 'O';
	public static int r = 0;
	public static int c = 0;
	public static int row;
	public static int col;
	public static int count = 0;
	public static int endgame = 0;
	public static char playAgain='Y';

	public static void showWelcome() {
		System.out.println("***** Welcome to OX Game *****");
	}
	public static void showTable(char[][] XOgame) {
		for (int i = 0; i < 3; i++) {
			System.out.println("-------------");
			for (int j = 0; j < 3; j++) {
				System.out.print("| " + XOgame[i][j]);
				System.out.print(" ");
			}
			System.out.print("|");
			System.out.println();
		}
		System.out.println("-------------");
	}

	

	public static void inputFunc() {
		System.out.print("Please input row and collum : ");
		row = kb.nextInt();
		col = kb.nextInt();
		row--;
		col--;
		count++;
                setTable();		
	}
        public static void setTable(){
            if(errorFunc() == true) {
			inputFunc();
		} else {
			XOgame[row][col] = turn;
		}
        }
        private static void turnFunc() {
		System.out.println("Turn : " + turn);
	}
	public static void showTurn() {
		if (turn == 'O') {
                    turn = 'X';
		} else if (turn == 'X') {
                    turn = 'O';
		}
	}
	private static boolean errorFunc() {
            for(int i = 0 ;i < 3 ;i++){
                if(errorRow()){
                    return true;
                }else if(errorCol()){
                    return true;
                }else if(errorRowCol()){
                    return true;
                }
            } 
            return false;
        }
        public static boolean errorRow() {
            if (row > 2 || row < 0) {
                System.out.println("ERROR , Please input 1 or 2 or 3");
                System.out.println("You enter a number exceeded, please try again.");
                return true;
            }
            return false;
            } 
        public static boolean errorCol() {
            if (col > 2 || col < 0) {
                System.out.println("ERROR , Please input 1 or 2 or 3");
                System.out.println("You enter a number exceeded, please try again.");
                return true;
            }
            return false;
            }
        public static boolean errorRowCol(){
            if (XOgame[row][col] == 'O' || XOgame[row][col] == 'X') {
                System.out.println("Please choose another Row and Collum!!");
                System.out.println("Please enter a new number.");
                return true;
            } else {
                    return false;
            }
	}

	 public static boolean Play() {
            for(int i = 0 ;i < 3 ;i++){
                if(checkRow()){
                    return true;
                }else if(checkCol()){
                    return true;
                }else if(checkDiagonal()){
                    return true;
                }
            } 
            return false;
         }
        public static boolean checkCol() {
            for (int r = 0; r < 3; r++) {
                if (XOgame[0][r] == turn && XOgame[1][r] == turn && XOgame[2][r] == turn) {
                    showWin();
                    return true;
                }
                
             }return false;
        }
        public static boolean checkRow() {
            for (int r = 0; r < 3; r++) {
                if (XOgame[r][0] == turn && XOgame[r][1] == turn && XOgame[r][2] == turn) {
                    showWin();
                    return true;
                }
            } 
            return false;
        }
        public static boolean checkDiagonal() {
            if(checkDiagonal1()) {
                return true;
            }if(checkDiagonal2()) {
                return true;
            }
            return false;
        }
        public static boolean checkDiagonal1() {
            for(int i = 0 ;i < 3 ;i++){
                if (XOgame[0][0] == turn && XOgame[1][1] == turn && XOgame[2][2] == turn) {
                showWin();
                return true;
                } 
            }return false;
	}
        public static boolean checkDiagonal2() {
            for(int i = 0 ;i < 3 ;i++){
                if (XOgame[0][2] == turn && XOgame[1][1] == turn && XOgame[2][0] == turn) {
                showWin();
                return true;
                }
            }return false;
	}
	private static boolean checkDraw() {
		if (count == 9) {
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            System.out.print(XOgame[i][j]);
                            System.out.print(" ");
                        }
                        System.out.println();
                    }
                    System.out.println("It is Draw!!");
                    return true;
		}
		return false;
	}
        public static void showMessage(){
            System.out.println("This position is already filled etc.");
        }
        public static void CheckEmptyO(){
            if (XOgame[row][col]==('O') || XOgame[row][col]==('X')) {
                showMessage();
                showTurn();
            }else {
                XOgame[row][col] = ('O');
                count++;
            }
        }

	private static void showWin() {
		for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                            System.out.print(XOgame[i][j]);
                            System.out.print(" ");
                    }
                    System.out.println();
		}
		System.out.println("Player " + turn + " Winner!!");
        } 
        public static void showPlayagain(){
            System.out.println("Game is over. Do you want to play again?");
            System.out.print("Please input Y/N : ");
        }
        private static void checkRestar() {
            if (endgame != 0) {
                playAgain = kb.next().charAt(0);
                if (playAgain == 'N'||playAgain == 'n') {
                    System.out.println("Good Bye...");
                } else if (playAgain == 'Y'||playAgain == 'y') {
                    while (endgame == 0) {
                        for (int i = 0; i < 3; i++) {
                            for (int j = 0; j < 3; j++) {
                               Play();
                               XOgame[i][j] = '-';
                               break;
                            }
                        }
                    }
                showEnd();
            }
        }
        }
        private static void showEnd() {
		System.out.println("Thak you bye bye.....");
        }
	public static void main(String[] args) {
		showWelcome();
		for (;;) {
			showTable(XOgame);
			turnFunc();
			inputFunc();
			if (Play() == true) {
                            showPlayagain();
                            checkRestar();
				break;
			}else if (checkDraw() == true) {
                            showPlayagain();
                            checkRestar();
                                break;
                        }
                        showTurn();     
		}
        }
	
}
